dbeacon (0.4.0-2) unstable; urgency=medium

  [ Ana C. Custura ]
  * Adds service file for running dbeacon as user dbeacon

 -- Iain R. Learmonth <irl@debian.org>  Tue, 24 Jan 2017 21:09:54 +0000

dbeacon (0.4.0-1) unstable; urgency=medium

  * Imported Upstream version 0.4.0
  * Removed rfc3542-ipv6recvpktinfo patch, applied upstream
  * Added format-security.patch:
   - Fixes an unsafe call to syslog() in dbeacon.cpp.
  * debian/control:
   - Maintainer now set to the Internet Measurement Packaging Team with
     myself as uploader.
   - Debhelper version bumped to 9, compat level set to 9.
   - Standards version bumped to 3.9.8.
   - Set a homepage for the upstream repository
   - Set Vcs-* fields for the pkg-netmeasure VCS.

 -- Iain R. Learmonth <irl@debian.org>  Mon, 20 Jun 2016 22:47:26 +0100

dbeacon (0.3.9.3-2) unstable; urgency=low

  * Add a patch that modified a setsockopt() argument to re-enable ssmping
    functionality with IPv6, broken by newer kernels.
  * Change the init script to support multiple dbeacon instances, started
    using configs from /etc/dbeacon/*.conf. Since old installations used
    /etc/dbeacon/dbeacon.conf, this is backwards-compatible.
  * Add support for the status action in the init script.
  * Remove Vcs-Svn & Vcs-Browser, since they don't exist anymore.
  * Minor fixes to the package's description, thanks to lintian.
  * Remove obsolete Depends/Recommends.
  * Update to Standards-Version 3.9.2, no changes needed.

 -- Faidon Liambotis <paravoid@debian.org>  Fri, 23 Dec 2011 14:29:59 +0200

dbeacon (0.3.9.3-1) unstable; urgency=low

  * Switch to debhelper 7 & dh.
  * Add ${misc:Depends} to Depends.
  * Use Vcs-* headers instead of the deprecated XS-Vcs ones.
  * Require $remote_fs on init script's LSB headers, since we use /usr (thanks
    lintian!)
  * Update to Standards-Version 3.8.4.
  * Switch to dpkg-source 3.0 (quilt) format to use an orig.tar.bz2 among
    other things.

 -- Faidon Liambotis <paravoid@debian.org>  Wed, 10 Feb 2010 17:45:22 +0200

dbeacon (0.3.9.1-1) unstable; urgency=low

  * First upload to Debian (Closes: #430622).
  * Several packaging changes.

 -- Faidon Liambotis <paravoid@debian.org>  Mon,  2 Jul 2007 11:39:13 +0300

dbeacon (0.3.9-1) unstable; urgency=low

  * Updated to 0.3.9.

 -- Hugo Santos <hugo@fivebits.net>  Fri, 17 Feb 2006 21:16:23 +0000

dbeacon (0.3.8) unstable; urgency=low

  * Updated to 0.3.8.

 -- Hugo Santos <hugo@fivebits.net>  Mon,  5 Sep 2005 13:05:14 +0100

dbeacon (0.3.7) unstable; urgency=low

  * Updated to 0.3.7.

 -- Hugo Santos <hugo@fivebits.net>  Wed, 10 Aug 2005 16:31:56 +0100

dbeacon (0.3.6) unstable; urgency=low

  * Updated to 0.3.6.

 -- Hugo Santos <hugo@fivebits.net>  Tue, 26 Jul 2005 23:07:38 +0100

dbeacon (0.3.5) unstable; urgency=low

  * Upstream version update.

 -- Hugo Santos <hugo@fivebits.net>  Sun, 10 Apr 2005 02:19:57 +0100

dbeacon (0.3.2) unstable; urgency=low

  * Another version bump.

 -- Hugo Santos <hugo@fivebits.net>  Thu,  7 Apr 2005 12:42:18 +0100

dbeacon (0.3.1) unstable; urgency=low

  * Bumped version

 -- Hugo Santos <hugo@fivebits.net>  Wed,  6 Apr 2005 02:25:01 +0100

dbeacon (0.3) unstable; urgency=low

  * Bumped version

 -- Hugo Santos <hugo@fivebits.net>  Tue,  5 Apr 2005 01:39:52 +0100

dbeacon (0.1-1) unstable; urgency=low

  * Initial Release.

 -- Hugo Santos <hugo@fivebits.net>  Sun,  19 Mar 2005 18:19:00 +0000

